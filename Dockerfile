# Based on https://bmaingret.github.io/blog/2021-11-15-Docker-and-Poetry

ARG APP_NAME=aml_final_project
ARG APP_COPY_PATH=/opt/$APP_NAME
ARG PYTHON_VERSION=3.10.10
ARG POETRY_VERSION=1.4.2

FROM python:3.10.10 as staging
ARG APP_NAME
ARG APP_COPY_PATH
ARG PYTHON_VERSION
ARG POETRY_VERSION

ENV \
    PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1 \
    PYTHONFAULTHANDLER=1 
ENV \
    POETRY_VERSION=$POETRY_VERSION \
    POETRY_HOME="/opt/poetry" \
    POETRY_VIRTUALENVS_IN_PROJECT=true \
    POETRY_NO_INTERACTION=1

# Install Poetry - respects $POETRY_VERSION & $POETRY_HOME
RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py | python
ENV PATH="$POETRY_HOME/bin:$PATH"

WORKDIR $APP_COPY_PATH

FROM staging as development
ARG APP_NAME
ARG APP_COPY_PATH

WORKDIR $APP_COPY_PATH

# Copy in the config files:
COPY pyproject.toml poetry.lock ./
# Install only dependencies:
RUN poetry install --no-root --only main

# Copy in everything else and install:
COPY aml_final_project/ ./aml_final_project
COPY tests/ ./tests
COPY README.md ./
RUN poetry install --only main

# Install tmux
ENV DEBIAN_FRONTEND=noninteractive
RUN apt update && apt install -y tmux && rm -rf /var/lib/apt/lists/*
