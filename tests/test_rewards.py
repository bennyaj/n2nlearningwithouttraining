from aml_final_project.reward_functions import rdm, compression_reward, teacher_guidance
from nas-without-training import scores

import numpy as np
import logging

import torch
from torch import tensor

from math import isclose

def test_rdm():
    activations = tensor([
        [0, 1, 2], 
        [2, 1, 0]
    ])
    print(torch.cov(activations))
    assert torch.equal(
        rdm(activations), 
        tensor([
            [0, 2],
            [2, 0],
        ])
    )

    activations = tensor([
        [-0.2678, -0.0908, -0.3766,  0.2780],
        [-0.5812,  0.1535,  0.2387,  0.2350]
    ])
    activations_rdm = rdm(activations)
    assert np.allclose(
        activations_rdm.numpy(), 
        np.array([
            [0, 0.6418], 
            [0.6418, 0]
        ]),
        atol=0.001
    )

def test_compression_reward():
    logging.info('No compression, 100% accuracy ratio')
    reward = compression_reward(1000, 1000, 1.0, 1.0)
    assert isclose(reward, 0.0)

    logging.info('25% compression, 100% accuracy ratio')
    reward = compression_reward(1000, 750, 1.0, 1.0)
    assert isclose(reward, 0.4375)

    logging.info('90% compression, 25% accuracy ratio')
    reward = compression_reward(1000, 100, 1.0, 0.25)
    assert isclose(reward, 0.2475)

    logging.info('Near 100% compression, 25% accuracy ratio')
    reward = compression_reward(1000000, 1, 1.0, 0.25)
    assert isclose(reward, 0.25)

def test_teacher_guidance():
    # 3 layers, 5 inputs, 4 activations per input
    teacher_activations = torch.randn((3, 5, 4), dtype=torch.float64)
    logging.info(f'Teacher Activations: {teacher_activations}')

    # 2 layers, 5 inputs, 3 activations per input
    student_activations = torch.randn((2, 5, 3), dtype=torch.float64)
    logging.info(f'Student Activations: {student_activations}')

    tg = teacher_guidance(teacher_activations, student_activations)
    logging.info(f'Teacher Guidance Score: {tg}')

def test_mellor():
    assert(False)



    



