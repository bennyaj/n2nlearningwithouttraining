from copy import deepcopy

import torch
import torch.nn as nn
from torch import Tensor

import logging

from aml_final_project.third_party.vgg import make_layers, cfgs, VGG

# From here: https://stackoverflow.com/questions/54846905/pytorch-get-all-layers-of-model
def get_children(model):
    # get children form model!
    children = list(model.children())
    flatt_children = []
    if children == []:
        # if model has no children; model is last child! :O
        return model
    else:
       # look for children from children... to the last child!
       for child in children:
            try:
                flatt_children.extend(get_children(child))
            except TypeError:
                flatt_children.append(get_children(child))
    return flatt_children

# Represents the state of an individual layer in the MDP network state
class LayerState:
    def __init__(self, layer_cfg, in_channels):
        self.layer_cfg = layer_cfg
        self.in_channels = in_channels

        self.layer_type = 0

        self.kernel_width = 0
        self.kernel_height = 0

        self.stride_width = 0
        self.stride_height = 0

        self.padding_width = 0
        self.padding_height = 0

        self.out_channels = 0

        self.skip_start = 0
        self.skip_end = 0

        # MaxPool2D case
        if layer_cfg == 'M':
            self.layer_type = 0

            self.kernel_width = 2
            self.kernel_height = 2

            self.stride_width = 2
            self.stride_height = 2

            self.out_channels = in_channels
        # Conv2D case
        else:
            self.layer_type = 1

            self.kernel_width = 3
            self.kernel_height = 3

            self.padding_width = 1
            self.padding_height = 1

            self.out_channels = layer_cfg

    def tensor(self):
        tensor = Tensor([
            self.layer_type,
            self.kernel_width,
            self.kernel_height,
            self.stride_width,
            self.stride_height,
            self.padding_width,
            self.padding_height,
            self.out_channels,
            self.skip_start,
            self.skip_end
        ]).unsqueeze(0)
        tensor.requires_grad = True

        return tensor

# Abstraction around the models that are being compressed. Contains both the
# model architecture itself and a representation of its current state
class Network:
    def __init__(self, model_cfg):
        self.model_cfg = model_cfg

        self.layer_states = []

        # Iterate over the layers of the model and store a simplified state
        # of their architectures
        prev_channels = 3
        for layer_cfg in model_cfg:
            self.layer_states.append(LayerState(layer_cfg, prev_channels))
            prev_channels = self.layer_states[-1].out_channels

        logging.debug(f"Layer States:")
        for layer_state in self.layer_states:
            logging.debug(f"{layer_state.tensor()}")
            
        # Used to track all transformations that have been applied to this network
        self.queued_remove_actions = []
        self.queued_shrink_actions = []
        self.applied_remove_actions = []
        self.applied_shrink_actions = []

    def queue_remove_layer(self, layer_index):
        self.queued_remove_actions.append(layer_index)

    def queue_shrink(self, layer_index, shrink_parameters):
        self.queued_shrink_actions.append((layer_index, shrink_parameters))

    # Applies actions (remove, then shrink) that are in the queue
    def apply_actions(self):
        logging.debug(f"Applying remove actions {self.queued_remove_actions}")

        # Remove the layers that were queued for removal
        self.model_cfg = [self.model_cfg[i] for i, layer_state in enumerate(self.layer_states) if i not in self.queued_remove_actions]
        self.layer_states = [layer_state for i, layer_state in enumerate(self.layer_states) if i not in self.queued_remove_actions]

        # TODO: Shrink

        self.applied_remove_actions = self.queued_remove_actions
        self.queued_remove_actions = []

        logging.debug(f"Layer States:")
        for layer_state in self.layer_states:
            logging.debug(f"{layer_state.tensor()}")

    def generate_model(self):
        return VGG(make_layers(self.model_cfg, batch_norm=True))

    def num_parameters(self):
        return sum(1 for parameter in self.model.parameters())
