import torch
from torch import corrcoef, Tensor, triu_indices
import numpy as np
import math
import logging 

# Computes the dissimilarity matrix for a single batch of model activations
# activations: n by m matrix where n is the number of inputs in the batch
# and m is the number of outputs in each activation
def rdm(activations):
    return 1 - torch.corrcoef(activations)

# For each input
    # Teacher predicts, get all of its activations across all layers
    # Compute RDM of teacher activations at each layer
    #
    # Student predicts, get all of its activations across all layers
    # Compute RDM of student activations at each layer
    #
    # For each Teacher RDM
        # For each Student RDM
            # Compute correlation between Teacher and Student RDM upper triangles
        # Save the max correlation between any Student layer and this Teacher layer
    # Take the average of the max correlations to get "Teacher Guidance" for a single input
# Take the average teacher guidance across all inputs to get a final score?
def teacher_guidance(teacher_layer_activations, student_layer_activations):
    teacher_layers = len(teacher_layer_activations)
    student_layers = len(student_layer_activations)

    inputs = teacher_layer_activations[0].shape[0]

    # Precompute flattened RDMs for every layer
    # Note that all of these are inputs x inputs in dimension
    rdm_ut_indices = triu_indices(inputs, inputs, offset=1)
    rdms = [rdm(student_activations)[rdm_ut_indices[0],rdm_ut_indices[1]] for student_activations in student_layer_activations]
    rdms.extend([rdm(teacher_activations)[rdm_ut_indices[0],rdm_ut_indices[1]] for teacher_activations in teacher_layer_activations])

    for student_activations in student_layer_activations:
        logging.debug(f"Activations: {student_activations}")
        logging.debug(f"RDM: {rdm(student_activations)}")

    # Put all of the flatted RDMs into a single tensor
    # Note that the student layers are on top
    rdms = torch.stack(rdms)
    logging.debug(f"RDM: {rdms}")

    # Get correlations between all rows. This will provide a correlation between
    # every student layer's RDM and every teacher layer's RDM 
    rdm_correlations = torch.corrcoef(rdms)
    logging.debug(f"RDM correlations: {rdm_correlations}")

    # Extract the maximum RDM correlation with a teacher layer for each student layer
    student_layer_scores = torch.max(
        rdm_correlations[:,student_layers:],
        dim=1,
        )
    logging.debug(f"Student Layer Scores: {student_layer_scores}")

    # Overall teacher guidance score is just the mean layer score
    return student_layer_scores.values.mean()

# Implements the reward function in section 3.3 of Ashok and a variant
def compression_reward(teacher_parameters, student_parameters, teacher_accuracy, student_accuracy, max_parameters=math.inf):
    # Penalize any model that's exactly the same as the teacher
    if teacher_parameters == student_parameters:
        return -1

    compression_ratio = 1 - (student_parameters / (teacher_parameters))

    logging.debug(f'Compression Ratio: {compression_ratio}')

    accuracy_ratio = student_accuracy / teacher_accuracy
    logging.debug(f'Accuracy Ratio: {accuracy_ratio}')

    reward = compression_ratio * (2 - compression_ratio) * accuracy_ratio

    return reward
