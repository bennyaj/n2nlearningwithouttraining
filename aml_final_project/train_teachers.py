import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim.lr_scheduler import MultiStepLR

import torchvision
from torchvision.models import vgg19, resnet18
from torchvision.transforms import ToTensor
from torchvision import transforms

import argparse
import numpy as np
import random

from PIL import Image

import matplotlib.pyplot as plt
import numpy as np

from tqdm import tqdm

from aml_final_project.third_party.resnet import resnet18
from aml_final_project.third_party.vgg import vgg13_bn, vgg19_bn
from aml_final_project.datasets import get_svhn, get_cifar10

import os
from pathlib import Path

import logging

def main():
    parser = argparse.ArgumentParser(prog='Model Compressor')

    parser.add_argument('model', choices=['vgg13_bn', 'vgg19_bn'])
    parser.add_argument('dataset', choices=['cifar10', 'svhn'])
    parser.add_argument('--num-workers', type=int, default=8)
    parser.add_argument('--data-path', default='./data')
    parser.add_argument('--model-path', default='./models')
    parser.add_argument('--checkpoint-path')
    parser.add_argument('--debug', action='store_true')

    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
    logging.info(device)

    logging.info(f"Training {args.model}")

    # TODO: Training is hard, figure out why this is so hard to train and use pretrained for now
    pretrained = args.dataset == 'cifar10'

    # Load the model
    model = None
    if args.model == 'vgg13_bn':
        model = vgg13_bn(device=device, pretrained=pretrained)
    if args.model == 'vgg19_bn':
        model = vgg19_bn(device=device, pretrained=pretrained)

    epochs = None
    learning_rate = None
    optimizer = None
    scheduler = None
    train_dataloader = None
    valid_dataloader = None
    logging.info(f"Training on dataset {args.dataset}")

    # Load the dataset
    if args.dataset == 'cifar10':
        epochs, learning_rate, optimizer, scheduler, train_dataloader, valid_dataloader = get_cifar10(model, args.data_path, args.num_workers)
    elif args.dataset == 'svhn':
        epochs, learning_rate, optimizer, scheduler, train_dataloader, valid_dataloader = get_svhn(model, args.data_path, args.num_workers)

    start_epoch = 0
    if args.checkpoint_path:
        checkpoint = torch.load(args.checkpoint_path)
        model.load_state_dict(checkpoint['model_state_dict'])
        optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
        scheduler.load_state_dict(checkpoint['scheduler_state_dict'])
        start_epoch = checkpoint['epoch'] + 1
        train_epoch_loss = checkpoint['training_loss'][-1]
        valid_epoch_loss = checkpoint['validation_loss'][-1]
        train_epoch_acc = checkpoint['training_accuracy'][-1]
        valid_epoch_acc = checkpoint['validation_accuracy'][-1]
        logging.info(f'Resuming from checkpoint {args.checkpoint_path} at epoch {start_epoch}')
        logging.info(f"Training loss: {train_epoch_loss:.3f}, training acc: {train_epoch_acc:.3f}")
        logging.info(f"Validation loss: {valid_epoch_loss:.3f}, validation acc: {valid_epoch_acc:.3f}")
        logging.info('-'*50)

    criterion = nn.CrossEntropyLoss()

    model.to(device)

    train_loss, valid_loss = [], []
    train_acc, valid_acc = [], []
    for epoch in range(start_epoch, epochs):
        logging.info(f"Epoch {epoch + 1} of {epochs} for {args.model} on {args.dataset}")    

        train_epoch_loss, train_epoch_acc = train(
            model,
            train_dataloader,
            optimizer,
            scheduler,
            criterion,
            device
        )
        valid_epoch_loss, valid_epoch_acc = validate(
            model, 
            valid_dataloader, 
            criterion,
            device
        )
        train_loss.append(train_epoch_loss)
        valid_loss.append(valid_epoch_loss)
        train_acc.append(train_epoch_acc)
        valid_acc.append(valid_epoch_acc)

        logging.info(f"Training loss: {train_epoch_loss:.3f}, training acc: {train_epoch_acc:.3f}")
        logging.info(f"Validation loss: {valid_epoch_loss:.3f}, validation acc: {valid_epoch_acc:.3f}")
        logging.info('-'*50)

        if epoch % (epochs / 10) == 0 or epoch == epochs - 1 :
            logging.info("Saving model checkpoint")

            Path(f'{args.model_path}/{args.model}/{args.dataset}').mkdir(parents=True, exist_ok=True)
            torch.save({
                'epoch': epoch,
                'model_state_dict': model.state_dict(),
                'optimizer_state_dict': optimizer.state_dict(),
                'scheduler_state_dict': scheduler.state_dict(),
                'training_loss': train_loss,
                'validation_loss': valid_loss,
                'training_accuracy': train_acc,
                'validation_accuracy': valid_acc
                }, f'{args.model_path}/{args.model}/{args.dataset}/checkpoint_{epoch}.tar'
            )

# Runs one epoch of training
def train(model, trainloader, optimizer, scheduler, criterion, device):
    # Turn on training mode
    model.train()

    train_running_loss = 0.0
    train_running_correct = 0
    counter = 0
    for i, data in tqdm(enumerate(trainloader), total=len(trainloader)):
        counter += 1
        image, labels = data
        image = image.to(device)
        labels = labels.to(device)
        optimizer.zero_grad()
        # Forward pass.
        outputs = model(image)
        # Calculate the loss.
        loss = criterion(outputs, labels)
        train_running_loss += loss.item()
        # Calculate the accuracy.
        _, preds = torch.max(outputs.data, 1)
        train_running_correct += (preds == labels).sum().item()
        # Backpropagation
        loss.backward()
        # Update the weights.
        optimizer.step()
    scheduler.step()
    
    # Loss and accuracy for the complete epoch.
    epoch_loss = train_running_loss / counter
    # epoch_acc = 100. * (train_running_correct / len(trainloader.dataset))
    epoch_acc = 100. * (train_running_correct / len(trainloader.dataset))
    return epoch_loss, epoch_acc

def validate(model, testloader, criterion, device):
    model.eval()
    logging.info('Validation')
    valid_running_loss = 0.0
    valid_running_correct = 0
    counter = 0
    with torch.no_grad():
        for i, data in tqdm(enumerate(testloader), total=len(testloader)):
            counter += 1
            
            image, labels = data
            image = image.to(device)
            labels = labels.to(device)
            # Forward pass.
            outputs = model(image)
            # Calculate the loss.
            loss = criterion(outputs, labels)
            valid_running_loss += loss.item()
            # Calculate the accuracy.
            _, preds = torch.max(outputs.data, 1)
            valid_running_correct += (preds == labels).sum().item()
        
    # Loss and accuracy for the complete epoch.
    epoch_loss = valid_running_loss / counter
    epoch_acc = 100. * (valid_running_correct / len(testloader.dataset))
    return epoch_loss, epoch_acc

if __name__ == '__main__':
    main()
