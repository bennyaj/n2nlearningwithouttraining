import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim import Adam, SGD

import torchvision
from torchvision.models import vgg19, resnet18
from torchvision.transforms import ToTensor
from torchvision import transforms

import argparse
import numpy as np
import random

import numpy as np

from tqdm import tqdm

from aml_final_project.third_party.resnet import resnet18
from aml_final_project.third_party.vgg import vgg13_bn, vgg19_bn, cfgs
from aml_final_project.network import Network
from aml_final_project.policies import RemovePolicy, ShrinkPolicy
from aml_final_project.training import train_policy
from aml_final_project.datasets import get_svhn, get_cifar10

import os
from pathlib import Path

import logging

def main():
    parser = argparse.ArgumentParser(prog='Model Compressor')

    parser.add_argument('model', choices=['vgg13_bn', 'vgg19_bn', 'resnet18'])
    parser.add_argument('model_weights')
    parser.add_argument('dataset', choices=['cifar10', 'svhn'])
    parser.add_argument('--num-workers', type=int, default=8)
    parser.add_argument('--batch-size', type=int, default=5)
    parser.add_argument('--data-path', default='./data')
    parser.add_argument('--model-path', default='./models')
    parser.add_argument('--remove-checkpoint')
    parser.add_argument('--epochs', default=100, type=int)
    parser.add_argument('--kd-epochs', default=5, type=int)
    parser.add_argument('--bashivan', action='store_true')
    parser.add_argument('--mellor', action='store_true')
    parser.add_argument('--debug', action='store_true')

    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
    logging.info(f"Running on device: {device}")

    # Load the model
    model = None

    logging.info(f"Training {args.model}")
    if args.model == 'vgg13_bn':
        model_cfg = cfgs['vgg13_bn']
        model = vgg13_bn(device=device)
    if args.model == 'vgg19_bn':
        model_cfg = cfgs['vgg19_bn']
        model = vgg19_bn(device=device)

    epochs = None
    learning_rate = None
    optimizer = None
    scheduler = None
    train_dataloader = None
    valid_dataloader = None
    logging.info(f"Training on dataset {args.dataset}")

    # Load the dataset
    if args.dataset == 'cifar10':
        _, _, _, _, train_dataloader, valid_dataloader = get_cifar10(model, args.data_path, args.num_workers)
    elif args.dataset == 'svhn':
        _, _, _, _, train_dataloader, valid_dataloader = get_svhn(model, args.data_path, args.num_workers)

    start_epoch = 0

    checkpoint = torch.load(args.model_weights)
    model.load_state_dict(checkpoint['model_state_dict'])
    start_epoch = checkpoint['epoch'] + 1
    train_epoch_loss = checkpoint['training_loss'][-1]
    valid_epoch_loss = checkpoint['validation_loss'][-1]
    train_epoch_acc = checkpoint['training_accuracy'][-1]
    valid_epoch_acc = checkpoint['validation_accuracy'][-1]
    logging.info(f'Getting model from checkpoint {args.model_weights} at epoch {start_epoch}')
    logging.info(f"Training loss: {train_epoch_loss:.3f}, training acc: {train_epoch_acc:.3f}")
    logging.info(f"Validation loss: {valid_epoch_loss:.3f}, validation acc: {valid_epoch_acc:.3f}")

    model.to(device)

    logging.info('Training Remove Policy')
    remove_policy = RemovePolicy()

    remove_optimizer = Adam(
        remove_policy.parameters(), 
        lr=0.1
    )
    
    remove_start_epoch = 0
    if args.remove_checkpoint:
        remove_checkpoint = torch.load(args.remove_checkpoint)
        remove_policy.load_state_dict(remove_checkpoint['model_state_dict'])
        remove_optimizer.load_state_dict(remove_checkpoint['optimizer_state_dict'])
        remove_start_epoch = remove_checkpoint['epoch'] + 1
        policy_loss = remove_checkpoint['policy_loss']
        logging.info(f'Resuming from remove_checkpoint {args.remove_checkpoint} at epoch {remove_start_epoch}')
        logging.info(f"Policy loss: {policy_loss:.3f}")
        logging.info('-'*50)

    train_policy(
        remove_policy, 
        model,
        model_cfg, 
        remove_optimizer,
        train_dataloader, 
        valid_dataloader, 
        device, 
        kd_epochs=args.kd_epochs, 
        batch_size=args.batch_size,
        epochs=args.epochs,
        use_bashivan=args.bashivan, 
        use_mellor=args.mellor, 
        debug=args.debug,
        save_path=f'{args.model_path}/remove_policy/{args.model}/{args.dataset}',
        start_epoch=remove_start_epoch
    )

if __name__ == '__main__':
    main()
