import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim.lr_scheduler import MultiStepLR

import torchvision
from torchvision.models import vgg19, resnet18
from torchvision.transforms import ToTensor
from torchvision import transforms


def get_cifar10(model, data_path, num_workers):
    # From here: https://stackoverflow.com/questions/69747119/pytorch-cifar10-images-are-not-normalized
    cifar10_mean = [0.4914, 0.4822, 0.4465]
    cifar10_std = [0.2470, 0.2435, 0.2616]

    # From Section 10.2 of Ashok
    epochs = 150
    learning_rate = 0.001
    optimizer = optim.SGD(model.parameters(), lr=learning_rate)
    scheduler = MultiStepLR(optimizer, milestones=[80, 120], gamma=0.1)

    cifar10_transform = transforms.Compose([
            transforms.RandomCrop(size=(32, 32), padding=4),
            transforms.RandomHorizontalFlip(),
            ToTensor(),
            transforms.Normalize(
                mean=cifar10_mean,
                std=cifar10_std
            )
    ])

    train_dataset = torchvision.datasets.CIFAR10(root=f'{data_path}', train=True, download=True, transform=cifar10_transform)
    train_dataloader = torch.utils.data.DataLoader(
        train_dataset, 
        batch_size=128,
        shuffle=True, 
        num_workers=num_workers
    )
    
    valid_cifar10_transform = transforms.Compose([
        ToTensor(),
        transforms.Normalize(
            mean=cifar10_mean,
            std=cifar10_std
        )
    ])

    valid_dataset = torchvision.datasets.CIFAR10(root=f'{data_path}', train=False, download=True, transform=valid_cifar10_transform)
    valid_dataloader = torch.utils.data.DataLoader(
        valid_dataset, 
        batch_size=128,
        shuffle=True, 
        num_workers=num_workers
    )

    return epochs, learning_rate, optimizer, scheduler, train_dataloader, valid_dataloader

def get_svhn(model, data_path, num_workers):
    # From Section 10.2 of Ashok
    epochs = 150
    learning_rate = 0.001
    optimizer = optim.SGD(model.parameters(), lr=learning_rate)
    scheduler = MultiStepLR(optimizer, milestones=[80, 120], gamma=0.1)

    svhn_transform = transforms.Compose([
            ToTensor(),
            # From Here: https://albumentations.ai/docs/autoalbument/examples/svhn/
            transforms.Normalize(
                mean=[0.4376821, 0.4437697, 0.47280442],
                std=[0.19803012, 0.20101562, 0.19703614]
            )
    ])

    train_dataset = torchvision.datasets.SVHN(root=f'{data_path}', split='train', download=True, transform=svhn_transform)
    train_dataloader = torch.utils.data.DataLoader(
        train_dataset, 
        batch_size=128,
        shuffle=True, 
        num_workers=num_workers
    )
    valid_dataset = torchvision.datasets.SVHN(root=f'{data_path}', split='test', download=True, transform=svhn_transform)
    valid_dataloader = torch.utils.data.DataLoader(
        valid_dataset, 
        batch_size=128,
        shuffle=True, 
        num_workers=num_workers
    )

    return epochs, learning_rate, optimizer, scheduler, train_dataloader, valid_dataloader
