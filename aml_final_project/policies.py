from copy import deepcopy

import torch
from torch import Tensor

from torch.nn import LSTM, Linear, Sigmoid
import torch.nn as nn

import logging

import random

from aml_final_project.network import Network

# Formulated as an MDP
# States = all possible reduced network architectures derived from the teacher
# Actions = Either remove a layer or reduce number of parameters in a layer
# Transition = Action converts archicture s to architecture s'
# Discount = 1
# Reward = ashok_reward or bashivan_reward 
class RemovePolicy(nn.Module):
    def __init__(self):
        super(RemovePolicy, self).__init__()

        self.hidden = None
        # 10 is the number of inputs in the LayerState
        # 30 is the number of hidden units in section 10.1 of Ashok
        # 2 is the number of hidden layers in section 10.1 of Ashok
        self.lstm = LSTM(10, 30, num_layers = 2, bidirectional=True)

        # Linear layer to convert the hidden units into a 1 element output
        # TODO: Should this be linear?
        self.downscaler = Linear(60, 1)

        self.sigmoid = Sigmoid()

    def policy_type(self):
        return "Remove"

    def forward(self, layer_state):
        if self.hidden is None:
            output, self.hidden = self.lstm(layer_state.tensor())
        else:
            output, self.hidden = self.lstm(layer_state.tensor(), self.hidden)

        # logging.debug(f"LSTM Output: {lstm_output}")
        # logging.debug(f"Softmax Output: {softmax_output}")
        output = self.downscaler(output)
        # logging.debug(f"Downscaler Output: {downscaled_output}")
        output = self.sigmoid(output)

        return output

    def get_remove_action(self, layer_state, exploration_weight):
        remove_probability = self(layer_state)
        # Add a random chance of exploring no matter how certain it is
        if random.random() < remove_probability: 
            return True, remove_probability 
        else:
            return False, remove_probability

    # Given a teacher network, give back a student network with layers removed
    def apply(self, teacher: Network, exploration_weight=0.15):
        logging.debug(f"Applying remove policy")
        candidate = deepcopy(teacher)

        action_probabilities = []

        for layer_index in range(len(teacher.layer_states)):
            remove, action_probability = self.get_remove_action(candidate.layer_states[layer_index], exploration_weight)
            action_probabilities.append(action_probability)

            # Apply the action to update s_t
            if remove:
                candidate.queue_remove_layer(layer_index)

        action_probabilities = torch.cat(action_probabilities)
        logging.info(f"Action probabilities: {action_probabilities}")

        # Actually update the candidate based on the queued actions
        candidate.apply_actions()

        # Reset the hidden state since we'll be starting a new action sequence
        self.hidden = None

        return candidate, action_probabilities

class ShrinkPolicy(nn.Module):
    def __init__(self):
        super(ShrinkPolicy, self).__init__()
        # 10 is the number of inputs in LayerState plus the previous shrink action
        # 50 is the number of hidden units in section 10.1 of Ashok
        # 2 is the number of hidden layers in section 10.1 of Ashok
        self.lstm = LSTM(20, 50, num_layers = 2)

        # Linear layer to convert the hidden units into a 10 element output
        # TODO: Should this be linear?
        self.downscaler = Linear(50, 10)

        self.softmax = Softmax()

    def policy_type(self):
        return "Shrink"

    def forward(self, layer_state, prior_action):
        lstm_input = torch.cat([layer_state.tensor(), prior_action])
        lstm_output, lstm_h_n, lstm_c_n = self.lstm(lstm_input)
        downscaled_output = self.downscaler(lstm_output)
        softmax_output = self.softmax(downscaled_output)

        return linear_output

    def get_shrink_action(self, prior_action, layer_state):
        action_probabilities = self(prior_action, layer_state)
        return torch.argmax(action_probabilities), torch.max(action_probabilities)

    # Given a teacher network, give back a student network with parameters shrunk
    def apply(self, teacher: Network):
        prior_candidate = teacher_network
        candidate = None

        # Iterate over all layers of the model
        for layer_index in range(len(teacher.layer_states)):
            shrink_parameters = self.get_shrink_action(prior_candidate)

            # Copy the previous network state (s_t-1) as a starting point
            # for the updated network state (s_t)
            candidate = deepcopy(prior_candidate)

            # Save a copy of that starting point for the next loop
            prior_candidate = deepcopy(candidate)

            # Apply the action to update s_t
            candidate.shrink_layer(layer_index, shrink_parameters)
        
        return candidate
