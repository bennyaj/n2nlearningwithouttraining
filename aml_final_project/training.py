import torch
import torch.nn as nn
from torch.optim import Adam, SGD
from torch.nn import MSELoss, CrossEntropyLoss
from torch import tensor

import logging

import math
from math import isclose

from statistics import fmean

from tqdm import tqdm

import time

from aml_final_project.network import Network

from pathlib import Path

from reward_functions import compression_reward, teacher_guidance

from torch_intermediate_layer_getter import IntermediateLayerGetter as MidGetter

from aml_final_project.third_party.vgg import make_layers, cfgs, VGG

import random
# Exponential moving average
def ema(current, prior_ema, multiplier):
    return prior_ema + (current - prior_ema) * (1 - multiplier)

# Lines 2 to 11 and 12 to 20 of Algorithm 1 in Ashok
# epochs is N_1 and N_2
# L_1 and L_2 are the number of layers in teacher
# kd_epochs is the number of epochs used to distill to each student
def train_policy(policy, teacher: nn.Module, teacher_cfg, optimizer, train_loader, test_loader, device, epochs=100, batch_size=5, kd_epochs=5, use_bashivan=False, use_mellor=True, debug=False, save_path='/mnt/models/remove', start_epoch=0):
    policy.train()

    loss_function = KDLoss()

    prior_ema = 0

    # Computed from 100 samples
    rewards_norm = 1540.6965

#   rewards_norm = 0
#   logging.info("Generating random samples to get normalization term")
#   for i in tqdm(range(100)):
#       random_cfg = [layer for layer in teacher_cfg if random.random() > 0.5]
#       random_model = VGG(make_layers(random_cfg, batch_norm=True))
#       random_model.to(device)
#       random_model.K = torch.zeros((128, 128), device=device)
#           
#       def counting_forward_hook(module, inp, out):
#           logging.debug(f"Forward Hook: {inp}")
#           if isinstance(inp, tuple):
#               inp = inp[0]
#           inp = inp.clone()
#           inp = inp.view(inp.size(0), -1)
#           x = (inp > 0).float()
#           K = x @ x.t()
#           K2 = (1.-x) @ (1.-x.t())
#           random_model.K = random_model.K + K + K2
#           logging.debug(f"Updating random_model {random_model.K}")

#       for name, module in random_model.named_modules():
#           if 'ReLU' in str(type(module)):
#               module.register_forward_hook(counting_forward_hook)

#       image, labels = next(iter(test_loader))
#       image = image.to(device)
#       labels = labels.to(device)
#       _ = random_model(image)

#       _, reward = torch.slogdet(random_model.K)
#       rewards_norm += reward
#   rewards_norm /= 100

    # Computed from 100 samples
    logging.info(f"Computed norm term of {rewards_norm}")

    # Total number of epochs of rollouts that we run during training
    max_reward = 1.0
    for epoch in range(start_epoch, epochs):
        logging.info(f"Training {policy.policy_type()} policy epoch {epoch}")

        # Reset optimizer gradients
        optimizer.zero_grad()
        total_loss = tensor([0.0], device=device)

        running_teacher_parameters = []
        running_student_parameters = []
        running_teacher_accuracy = []
        running_student_accuracy = []
        running_mellor = []
        running_reward = []
        running_trajectory_loss = []
        # Total number of trajectory rollouts that we run in a single batch
        for rollout in range(batch_size):
            reward = 0

            logging.info(f"Current rollout within the batch: {rollout}")

            try:
                teacher_network = Network(teacher_cfg)

                # Compress the teacher to create the student architecture
                # Keep trying until a valid model is generated
                student_network, action_probabilities = policy.apply(teacher_network)

                logging.info(f"Teacher Config: {teacher_cfg}")
                logging.info(f"Student Config: {student_network.model_cfg}")

                # Create a pytorch model from the architecture
                logging.debug(f"Generating Model")
                student = student_network.generate_model()
                student.to(device)

                student.K = torch.zeros((128, 128), device=device)
                def counting_forward_hook(module, inp, out):
                    if isinstance(inp, tuple):
                        inp = inp[0]
                    inp = inp.clone()
                    inp = inp.view(inp.size(0), -1)
                    x = (inp > 0).float()
                    K = x @ x.t()
                    K2 = (1.-x) @ (1.-x.t())
                    student.K = student.K + K + K2
                    
                num_relu = 0
                for name, module in student.named_modules():
                    if 'ReLU' in str(type(module)):
                        module.register_forward_hook(counting_forward_hook)
                        num_relu += 1

                if num_relu == 0:
                    logging.warn("No ReLU, assigning award of -1")
                    reward = -1

                if reward >= 0:
                    image, labels = next(iter(test_loader))
                    image = image.to(device)
                    labels = labels.to(device)
                    student_outputs = student(image)

                    _, reward = torch.slogdet(student.K)
                    reward /= rewards_norm
                    running_mellor.append(reward)
                    logging.debug(f"Mellor Reward: {reward}")
                    if reward > max_reward:
                        max_reward = reward
            except Exception as e:
                print(str(e))
                logging.warn("Bad model, assigning reward of -1")
                reward = tensor([-1], device=device)

            teacher_parameters = sum(1 for parameter in teacher.parameters())
            running_teacher_parameters.append(teacher_parameters)
            student_parameters = sum(1 for parameter in student.parameters())
            running_student_parameters.append(student_parameters)
            compression_term = (1 - (student_parameters / (teacher_parameters + 1)))
            logging.debug(f"Compression Score: {compression_term}")
            if reward >= 0:
                reward *= compression_term * (2 - compression_term)

            if not use_mellor or (epoch % 25 == 0 and epoch != 0 and rollout == batch_size - 1):
                logging.info(f"Running validation step at epoch {epoch}")
                try:
                    # Distill the teacher model into the compressed student architecture
                    student = distill_student(
                        teacher,
                        student,
                        train_loader,
                        device,
                        epochs=kd_epochs
                    ) 

                    logging.debug(f"Validating Student After Distilling")
                    student_loss, teacher_accuracy, student_accuracy, guidance = validate_student(
                        teacher,
                        student,
                        test_loader,
                        loss_function,
                        device,
                        # Train the distilled model on the ground truth labels
                        hard_label_weight=1.0,
                        # Guidance will be 0 if this is false, otherwise this will
                        # give an accurate teacher guidance score
                        use_bashivan=use_bashivan
                    )
                    running_teacher_accuracy.append(teacher_accuracy)
                    running_student_accuracy.append(student_accuracy)

                    logging.info(f"Validated student model with accuracy {student_accuracy} and loss {student_loss}")
                     
                    if not use_mellor:
                        teacher_parameters = sum(1 for parameter in teacher.parameters())
                        running_teacher_parameters.append(teacher_parameters)
                        student_parameters = sum(1 for parameter in student.parameters())
                        running_student_parameters.append(student_parameters)

                        if reward >= 0:
                            # Compute the reward for the trajectory.
                            # This is R_k in section 3.4 of Bashivan
                            reward = compression_reward(
                                teacher_parameters, 
                                student_parameters, 
                                teacher_accuracy,
                                student_accuracy,
                                use_bashivan=use_bashivan
                            ) + guidance
                except Exception as e:
                    print(str(e))
                    logging.warn("Bad model, can't validate, giving a reward of -1")
                    reward = tensor([-1], device=device)

            running_reward.append(reward)

            logging.debug(f"Reward: {reward}")

            logging.debug(f"Prior EMA: {prior_ema}")

            action_probabilities = action_probabilities.to(device)
            trajectory_loss = -(action_probabilities.log() * (reward - prior_ema)).sum()
            prior_ema = ema(reward, prior_ema, 0.9)

            running_trajectory_loss.append(trajectory_loss)
            logging.debug(f"Trajectory Loss: {trajectory_loss}")

            total_loss += trajectory_loss

        # Calculate new gradients
        policy_loss = total_loss / batch_size

        logging.info(f"\nPolicy Loss: {policy_loss}")
        logging.info(50 * "-")
        policy_loss.backward()

        # Apply new gradients
        optimizer.step()

        logging.info("Saving model checkpoint")

        Path(save_path).mkdir(parents=True, exist_ok=True)
        torch.save({
            'epoch': epoch,
            'model_state_dict': policy.state_dict(),
            'optimizer_state_dict': optimizer.state_dict(),
            'policy_loss': policy_loss,
            'running_teacher_accuracy': running_teacher_accuracy,
            'running_student_accuracy': running_student_accuracy,
            'running_teacher_parameters': running_teacher_parameters,
            'running_student_parameters': running_student_parameters,
            'running_mellor': running_mellor,
            'running_reward': running_reward,
            'running_trajectory_loss': running_trajectory_loss,
            'rewards_norm': rewards_norm
            }, f'{save_path}/checkpoint_{epoch}.tar'
        )



# Implementation of the loss functions in section 3.5 of Ashok
class KDLoss(nn.Module):
    def __init__(self):
        super(KDLoss, self).__init__()
        self.mse = MSELoss()
        self.cross_entropy = CrossEntropyLoss()

    def forward(self, student_logits, teacher_logits, labels, hard_label_weight=0.0):
        soft_label_loss = 0

        soft_label_loss = self.mse(student_logits, teacher_logits)

        hard_label_loss = 0
        if not isclose(hard_label_weight, 0.0):
            # Don't waste time computing this unless the weight is nonzero 
            hard_label_loss = self.cross_entropy(student_logits, labels)

        return hard_label_loss * hard_label_weight + soft_label_loss * (1 - hard_label_weight)

# Distills a teacher model's knowledge into a given student architecture
# TODO: Set the learning rate based on the data?
def distill_student(teacher: nn.Module, student: nn.Module, train_loader, device, hard_labels=False, epochs=5, soft_label_weight=1.0, learning_rate=0.001):
    logging.debug(f"Distilling student for {epochs} epochs")
    teacher.eval()
    student.train()

    optimizer = SGD(
        student.parameters(), 
        lr=learning_rate
    )

    # Select loss function for cases described in section 3.5 of Ashok
    kd_loss = KDLoss()

    for epoch in range(epochs):
        logging.debug(f"Distilling student, epoch {epoch}")

        logging.debug(f"Distillation: training student")
        train_loss, teacher_train_acc, student_train_acc = train_student(
            teacher,
            student,
            train_loader,
            optimizer,
            kd_loss,
            device
        )

        logging.info(f"Training loss: {train_loss:.3f}, Training Acc: {student_train_acc:.3f}")

    return student

def train_student(teacher, student, train_loader, optimizer, loss_function, device):
    teacher.eval()
    student.train()

    train_running_loss = 0.0
    teacher_running_correct = 0
    student_running_correct = 0
    counter = 0
    for i, data in tqdm(enumerate(train_loader), total=len(train_loader)):
        start = time.time()
        counter += 1
        
        image, labels = data
        image = image.to(device)
        labels = labels.to(device)
        # Forward pass.
        teacher_outputs = teacher(image)
        student_outputs = student(image)

        # Calculate the loss.
        loss = loss_function(student_outputs, teacher_outputs, labels)
        train_running_loss += loss.item()

        # Calculate the accuracy.
        _, teacher_preds = torch.max(teacher_outputs.data, 1)
        teacher_running_correct += (teacher_preds == labels).sum().item()

        _, student_preds = torch.max(student_outputs.data, 1)
        student_running_correct += (student_preds == labels).sum().item()

        # Compute gradients to move the student towards the teacher
        loss.backward()

        # Apply those gradients to update the student
        optimizer.step()

    logging.debug(f"Train computing accuracies")
    # Loss and accuracy for the complete epoch.
    epoch_loss = train_running_loss / counter
    teacher_acc = 100. * (teacher_running_correct / len(train_loader.dataset))
    student_acc = 100. * (student_running_correct / len(train_loader.dataset))
    return epoch_loss, teacher_acc, student_acc 


def validate_student(teacher, student, test_loader, loss_function, device, use_bashivan=False, hard_label_weight=0.0):
    teacher.eval()
    student.eval()

    counter = 0
    valid_running_loss = 0.0
    teacher_running_correct = 0
    student_running_correct = 0

    teacher_modules = { name: name for name, _ in teacher.named_modules() if hasattr(teacher, name) }
    logging.debug(f'Teacher Modules: {teacher_modules}')
    teacher_mid_getter = MidGetter(teacher, teacher_modules)
    student_modules = { name: name for name, _ in student.named_modules() if hasattr(student, name) }
    logging.debug(f'Student Modules: {student_modules}')    
    student_mid_getter = MidGetter(student, student_modules)

    running_tg = [ ]
    with torch.no_grad():
        for i, data in enumerate(test_loader):
            start = time.time()
            counter += 1
            
            image, labels = data
            image = image.to(device)
            labels = labels.to(device)
            # Forward pass.
            teacher_mids, teacher_outputs = teacher_mid_getter(image)
            student_mids, student_outputs = student_mid_getter(image)

            if use_bashivan:
                teacher_layers = [outputs.flatten() for _, outputs in teacher_mids.items()] + [teacher_outputs]
                student_layers = [outputs.flatten() for _, outputs in student_mids.items()] + [student_outputs]

                running_tg.append(teacher_guidance(teacher_layers, student_layers))
            else:
                running_tg.append(0)

            # Calculate the loss.
            # logging.debug(f"Validate computing loss {time.time() - start}")
            loss = loss_function(student_outputs, teacher_outputs, labels, hard_label_weight=hard_label_weight)
            valid_running_loss += loss.item()

            # Calculate the accuracy.
            # logging.debug(f"Validate computing preds {time.time() - start}")
            _, teacher_preds = torch.max(teacher_outputs.data, 1)
            teacher_running_correct += (teacher_preds == labels).sum().item()

            _, student_preds = torch.max(student_outputs.data, 1)
            student_running_correct += (student_preds == labels).sum().item()

    logging.debug(f"Valid computing accuracies")
    # Loss and accuracy for the complete epoch.
    epoch_loss = valid_running_loss / counter
    teacher_acc = 100. * (teacher_running_correct / len(test_loader.dataset))
    student_acc = 100. * (student_running_correct / len(test_loader.dataset))
    return epoch_loss, teacher_acc, student_acc, fmean(running_tg)
